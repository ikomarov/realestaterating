using Microsoft.Extensions.Logging;
using RealEstateRating.Application.UseCases.RealEstateAgentsTop;
using RealEstateRating.Domain.Exceptions;
using RealEstateRating.Domain.Models;

namespace RealEstateRating;

public class Cli
{
    private readonly IRealEstateRatingHandler _realEstateRatingHandler;
    private readonly ILogger<Cli> _logger;

    public Cli(IRealEstateRatingHandler realEstateRatingHandler, ILogger<Cli> logger)
    {
        _realEstateRatingHandler = realEstateRatingHandler;
        _logger = logger;
    }

    public async Task Run()
    {
        string city = "Amsterdam";
        Console.WriteLine($"Top of real estate agents in {city} by amount of properties on sale");
        await LoadAndPrintResult(city, OutdoorSpaceType.NotSelected);

        Console.WriteLine("");
        Console.WriteLine($"Top of real estate agents in {city} by amount of properties with garden on sale");
        await LoadAndPrintResult(city, OutdoorSpaceType.Garden);
    }

    private async Task LoadAndPrintResult(string city, OutdoorSpaceType type)
    {
        int rowsToClear = 0;

        try
        {
            await foreach (var ratingData in _realEstateRatingHandler.Handle(city, type))
            {
                ClearLastRows(rowsToClear);
                rowsToClear = PrintRealEstateAgentsTable(ratingData.Data);

                if (ratingData.AmountOfProcessedRecords.HasValue)
                    Console.Write($"Amount of processed records so far is: {ratingData.AmountOfProcessedRecords}. ");

                Console.Write("Data is still loading...");
            }
        }
        catch (DataLoadingException e)
        {
            _logger.LogError(e, "Exception occured during data loading for city: {city}, type: {type}", city, type);
            Console.WriteLine("\n An error occured during data loading");
            return;
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Exception occured during data loading for city: {city}, type: {type}", city, type);

            Console.WriteLine("\n Unknown error occured");
            return;
        }

        ClearCurrentRow();
    }
    
    private void ClearLastRows(int count)
    {
        int i = 0;
        while(i < count && Console.CursorTop - 1 > 0)
        {
            Console.SetCursorPosition(0, Console.CursorTop - 1);
            Console.Write(new string(' ', Console.WindowWidth));
            ++i;
        }
        
        Console.SetCursorPosition(0, Console.CursorTop);
    }
    
    private void ClearCurrentRow()
    {
        Console.SetCursorPosition(0, Console.CursorTop);
        Console.Write(new string(' ', Console.WindowWidth)); 
        Console.SetCursorPosition(0, Console.CursorTop);
    }

    int PrintRealEstateAgentsTable(IEnumerable<RealEstateAgent> data)
    {
        Console.WriteLine("|{0,10}|{1,50}|{2,10}|", "Id", "Name", "Count");

        int printedRows = 1;

        foreach (var item in data)
        {
            Console.WriteLine("|{0,10}|{1,50}|{2,10}|", item.Id, item.Name[..Math.Min(50, item.Name.Length)],
                item.CountOfPropertiesOnSale);
            ++printedRows;
        }

        return printedRows;
    }
}