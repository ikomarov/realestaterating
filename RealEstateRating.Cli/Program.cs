﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using RealEstateRating;
using RealEstateRating.Application;
using RealEstateRating.Infrastructure;
using RealEstateRating.Infrastructure.DataSources.Funda.Configurations;

var configuration = BuildConfiguration(args);

using (var sp = CreateServiceProvider(configuration))
{
    using (var scope = sp.CreateScope())
    {
        var cli = scope.ServiceProvider.GetRequiredService<Cli>();
        await cli.Run();
    }
}

static ServiceProvider CreateServiceProvider(IConfiguration configuration)
{
    var fundaConfigSection = configuration
        .GetSection("fundaApiConfig");
    var fundaUsageConfig = fundaConfigSection.GetSection("usageConfig");
    var fundaConfig = fundaConfigSection
        .Get<FundaApiConfig>();
    
    if (fundaConfig == null)
        throw new ArgumentException("Config of funda api has to be provided");

    var serviceProvider = new ServiceCollection()
        .AddLogging(builder => builder.SetMinimumLevel(LogLevel.Warning).AddDebug())
        .AddTransient<Cli>()
        .AddApplication()
        .AddInfrastructure(fundaConfig);

    serviceProvider.Configure<FundaApiUsageConfig>(fundaUsageConfig);
        

    return serviceProvider.BuildServiceProvider();
}

static IConfiguration BuildConfiguration(string[] args) => new ConfigurationBuilder()
    .SetBasePath(Directory.GetCurrentDirectory())
    .AddJsonFile("appsettings.json", optional:true , reloadOnChange:true)
    .AddEnvironmentVariables()
    .AddCommandLine(args)
    .Build();