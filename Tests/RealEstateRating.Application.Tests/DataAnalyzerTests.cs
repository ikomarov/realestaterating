using FluentAssertions;
using RealEstateRating.Application.DataAnalysis;
using RealEstateRating.Domain.Models;

namespace RealEstateRating.Application.Tests;

public class DataAnalyzerTests
{
    DataAnalyzer GetTarget()
    {
        return new DataAnalyzer();
    }
    
    [Test]
    public void TopByAmountOfProperties_EmptyCollection_DoesntThrow()
    {
        // Arrange
        var data = Array.Empty<RealEstateAgent>();
        var target = GetTarget();
        
        // Act
        Action action = () => target.TopByAmountOfProperties(10, data);

        // Assert
        action
            .Should()
            .NotThrow();
    }

    [Test]
    public void TopByAmountOfProperties_ValidCollection_TopElements()
    {
        // Arrange
        var top1 = new RealEstateAgent(1, "name 1", new[] { Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid() });
        var top2 = new RealEstateAgent(2, "name 2", new[] { Guid.NewGuid(), Guid.NewGuid() });
        var data = new[]
        {
            new RealEstateAgent(3, "name 3"),
            new RealEstateAgent(4, "name 4", new [] { Guid.NewGuid() }),
            top2,
            top1
        };
        var expected = new[] { top1, top2 };
        var target = GetTarget();
        
        // Act
        var result = target.TopByAmountOfProperties(2, data);

        // Assert
        result.Should().BeEquivalentTo(expected);
    }
    
    [Test]
    public void TopByAmountOfProperties_CollectionSizeSmallerThanTop_TopElements()
    {
        // Arrange
        var top1 = new RealEstateAgent(1, "name 1", new[] { Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid() });
        var top2 = new RealEstateAgent(2, "name 2", new[] { Guid.NewGuid(), Guid.NewGuid() });
        var data = new[] { top2, top1 };
        var expected = new[] { top1, top2 };
        
        var target = GetTarget();
        
        // Act
        var result = target.TopByAmountOfProperties(5, data);

        // Assert
        result.Should().BeEquivalentTo(expected);
    }
}