using FluentAssertions;
using RealEstateRating.Domain.Models;
using RealEstateRating.Infrastructure.DataStorage;

namespace RealEstateRating.Infrastructure.Tests.DataStorage;

public class RealEstateAgentInMemoryRepositoryTests
{
    RealEstateAgentInMemoryRepository GetTarget()
    {
        return new RealEstateAgentInMemoryRepository();
    }

    [Test]
    public void Add_EmptyStorage_DoesntThrow()
    {
        // Arrange
        var target = GetTarget();

        // Act
        Action action = () =>
            target.Add("amsterdam", OutdoorSpaceType.NotSelected, new RealEstateAgent(1, "name"));

        // Assert
        action
            .Should()
            .NotThrow();
    }
    
    [Test]
    public void Add_EmptyStorage_AddItem()
    {
        // Arrange
        var city = "amsterdam";
        var type = OutdoorSpaceType.NotSelected;
        var agent = new RealEstateAgent(1, "name");
        
        var target = GetTarget();
        
        // Act
        target.Add(city, type, agent);

        // Assert
        var result = target.GetAll(city, type);
        result
            .Should().HaveCount(1).And
            .OnlyContain(x => x.Id == agent.Id);
    }
    
    [TestCase("")]
    [TestCase(null)]
    public void Add_CityNotProvided_ThrowArgumentException(string city)
    {
        // Arrange
        var target = GetTarget();

        // Act
        Action action = () =>
            target.Add(city, OutdoorSpaceType.NotSelected, new RealEstateAgent(1, "name"));

        // Assert
        action
            .Should()
            .Throw<ArgumentException>();
    }
    
    [Test]
    public void AddRange_EmptyStorage_AddItems()
    {
        // Arrange
        var city = "amsterdam";
        var type = OutdoorSpaceType.NotSelected;
        var agents = new List<RealEstateAgent>
        {
            new RealEstateAgent(1, "name 1"),
            new RealEstateAgent(2, "name 2"),
        };
        
        var target = GetTarget();
        
        // Act
        target.AddRange(city, type, agents);

        // Assert
        var result = target.GetAll(city, type);
        result
            .Should().HaveCount(2).And
            .BeEquivalentTo(agents);
    }
    
    [TestCase("Amsterdam", OutdoorSpaceType.NotSelected,
        "Utrecht", OutdoorSpaceType.NotSelected)]
    [TestCase("Amsterdam", OutdoorSpaceType.Garden,
        "Amsterdam", OutdoorSpaceType.NotSelected)]
    [TestCase("Amsterdam", OutdoorSpaceType.RoofTerrace,
        "Amsterdam", OutdoorSpaceType.Garden)]
    public void Add_ItemsWithDifferentKeys_DoesntMixKeys(
        string cityToAdd, OutdoorSpaceType typeToAdd, string existingCity, OutdoorSpaceType existingType)
    {
        // Arrange
        var agent = new RealEstateAgent(1, "name 1");
        
        var target = GetTarget();
        target.Add(existingCity, existingType, new RealEstateAgent(2, "name 2"));
        
        // Act
        target.Add(cityToAdd, typeToAdd, agent);

        // Assert
        var result = target.GetAll(cityToAdd, typeToAdd);
        result
            .Should().HaveCount(1).And
            .OnlyContain(x => x.Id == agent.Id);
    }
    
}