# RealEstateRating
Determine rating lists of:
- top 10 real estate agents in Amsterdam that have the most objects listed for sale
- top 10 real estate agents in Amsterdam that have the most objects with garden listed for sale
## How to run
To run the app you need to build solution and start RealEstateRating.Cli.
In order to make calls to funda api apiKey have to be provided via ether
env variable

`fundaApiConfig:accessConfig:apiKey={apiKey}`

or argument during start of the app

`./RealEstateRating.Cli --fundaApiConfig:accessConfig:apiKey={apiKey}`

## How is it built
App contains 4 levels:
-  **RealEstateRating.Cli** - Presentation layer in form of console app
- **RealEstateRating.Application** - Application layer which contains use cases and application services
- **RealEstateRating.Domain** - Domain layer which contains abstractions and models
- **RealEstateRating.Infrastructure** - Infrastructure layer which holds responsibility of using external services, like apis, data storage, etc

## How is it functioning
### Data loading and aggregation
Main functionality of the app requires to load information about houses on sale from Funda api. Information is provided with pagination functionality. To speed the loading of data up I decided to load it in batches. Each batch loads multiple pages in parallel.
One of requirements of assignment was to mitigate and handle erros connected to work with api. And one of restrictions was connected to max error rate we are allowed to make
> If you perform too many API requests in a short period of time (>100 per minute)

To avoid "too many requests" issue I used approach of client side rate limiter with queue to throttle requests on side of my application.
Size of the queue is connected to size of the batch, so, even in case if we are breaking rate limits, the whole batch will be enqueed for execution after we get 'free window'.
To handle errors I utilized RetryStrategy with exponential backoff type, so even in case of getting error from the api we will retry to do failed request a few times before giving up and showing an error to user.

The amount of requests that needs to be done, to load all data needed is more than 100, it means that execution of program will take more than one minute (because of rate limit).
To keep user updated about the current state and to process and aggregate data as soon as it's possible I decided to choose IAsyncEnumerable approach. 

On each iteration of IAsyncEnumerable new batch of data(multiple pages):
1. Is being loaded and returned from FundaDataSource class.
2. Is being aggregated and saved into in-memory storage by DataIngestionService.
3. Currently available data is being processed by DataAnalyzer to build rating of real estate agents.
4. Currently available rating is shown to user by CLI level.

### Data analysis 
In order to build the rating I used Min-Heap data structure.
That allowed me to build rating in O(N Log(K)), where K is size of the rating list (10).