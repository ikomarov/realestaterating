using System.Net;
using System.Threading.RateLimiting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Http.Resilience;
using Polly;
using RealEstateRating.Domain.Interfaces;
using RealEstateRating.Infrastructure.DataSources.Funda;
using RealEstateRating.Infrastructure.DataSources.Funda.Configurations;
using RealEstateRating.Infrastructure.DataStorage;

namespace RealEstateRating.Infrastructure;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddInfrastructure(this IServiceCollection services, FundaApiConfig apiConfig)
    {
        services
            .AddFundaDataSource(apiConfig)
            .AddDataStorage();

        return services;
    }

    private static IServiceCollection AddDataStorage(this IServiceCollection services)
    {
        services.AddScoped<IRealEstateAgentRepository, RealEstateAgentInMemoryRepository>();

        return services;
    }

    private static IServiceCollection AddFundaDataSource(this IServiceCollection services, FundaApiConfig apiConfig)
    {
        
        services
            .AddHttpClient<IFundaHttpClient, FundaHttpClient>(x =>
            {
                x.BaseAddress = apiConfig.AccessConfig.FundaUrl;
            })
            .AddResilienceHandler("retry-pipeline", builder =>
            {
                builder
                    .AddRateLimiter(GetRateLimiter(apiConfig.UsageConfig))
                    .AddRetry(GetRetryStrategyOptions());

                builder.AddTimeout(TimeSpan.FromSeconds(120));
            });
        services.AddTransient<IDataSource, FundaDataSource>();

        return services;
    }

    private static RateLimiter GetRateLimiter(FundaApiUsageConfig apiUsageConfig) => 
        new SlidingWindowRateLimiter(new SlidingWindowRateLimiterOptions
        {
            Window = TimeSpan.FromSeconds(apiUsageConfig.WindowInSeconds),
            AutoReplenishment = true,
            PermitLimit = apiUsageConfig.PermitLimit,
            QueueLimit = apiUsageConfig.BatchSize,
            QueueProcessingOrder = QueueProcessingOrder.OldestFirst,
            SegmentsPerWindow = 12,
        });

    private static HttpRetryStrategyOptions GetRetryStrategyOptions()
    {
        var predicateBuilder = new PredicateBuilder<HttpResponseMessage>()
            .HandleResult(response => response.StatusCode != HttpStatusCode.OK);

        return new HttpRetryStrategyOptions
        {
            ShouldHandle = predicateBuilder,
            MaxRetryAttempts = 4,
            Delay = TimeSpan.FromSeconds(3),
            BackoffType = DelayBackoffType.Exponential
        };
    }
}
