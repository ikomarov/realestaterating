using RealEstateRating.Domain.Models;

namespace RealEstateRating.Infrastructure.DataStorage;

internal record StorageSegmentKey
{
    public string City { get; }
    public OutdoorSpaceType Type { get; }

    public StorageSegmentKey(string city, OutdoorSpaceType type)
    {
        if (String.IsNullOrEmpty(city))
            throw new ArgumentException($"{nameof(City)} can not be empty");
            
        City = city.ToLower();
        Type = type;
    }
}