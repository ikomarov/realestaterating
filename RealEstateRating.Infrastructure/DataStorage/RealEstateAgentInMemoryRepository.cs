using RealEstateRating.Domain.Interfaces;
using RealEstateRating.Domain.Models;

namespace RealEstateRating.Infrastructure.DataStorage;

public class RealEstateAgentInMemoryRepository : IRealEstateAgentRepository
{
    private readonly Dictionary<StorageSegmentKey, Dictionary<long, RealEstateAgent>> _data = new();
    
    public void Add(string city, OutdoorSpaceType type, RealEstateAgent agent)
    {
        var key = EnsureSegmentCreated(city, type);
        _data[key][agent.Id] = agent;
    }
    
    public void AddRange(string city, OutdoorSpaceType type, IEnumerable<RealEstateAgent> agents)
    {
        var key = EnsureSegmentCreated(city, type);

        foreach (var item in agents)
        {
            _data[key][item.Id] = item;
        }
    }

    public bool TryGet(string city, OutdoorSpaceType type, long id, out RealEstateAgent? agent)
    {
        var key = new StorageSegmentKey(city, type);
        if (_data.TryGetValue(key, out var segment) && 
            segment.TryGetValue(id, out var value))
        {
            agent = value;
            return true;
        }

        agent = null;
        return false;
    }

    public IEnumerable<RealEstateAgent> GetAll(string city, OutdoorSpaceType type)
    {
        var key = new StorageSegmentKey(city, type);
        if (!_data.TryGetValue(key, out var segment))
        {
            throw new KeyNotFoundException();
        }
        
        return segment.Select(x => x.Value).AsEnumerable();
    }
    
    public bool TryGetAll(string city, OutdoorSpaceType type, out IEnumerable<RealEstateAgent> data)
    {
        var key = new StorageSegmentKey(city, type);
        if (_data.TryGetValue(key, out var segment))
        {
            data = segment.Select(x => x.Value).AsEnumerable();
            return true;
        }

        data = Array.Empty<RealEstateAgent>();
        return false;
    }


    private StorageSegmentKey EnsureSegmentCreated(string city, OutdoorSpaceType type)
    {
        var key = new StorageSegmentKey(city, type);
        if (!_data.ContainsKey(key))
        {
            _data[key] = new Dictionary<long, RealEstateAgent>();
        }

        return key;
    }
}