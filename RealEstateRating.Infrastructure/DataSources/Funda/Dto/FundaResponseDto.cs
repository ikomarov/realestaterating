using System.Text.Json.Serialization;

namespace RealEstateRating.Infrastructure.DataSources.Funda.Dto;

public class FundaResponseDto
{
    [JsonPropertyName("AccountStatus")]
    public long AccountStatus { get; set; }

    [JsonPropertyName("EmailNotConfirmed")]
    public bool EmailNotConfirmed { get; set; }

    [JsonPropertyName("ValidationFailed")]
    public bool ValidationFailed { get; set; }

    [JsonPropertyName("Objects")]
    public PropertyDto[] Properties { get; set; }

    [JsonPropertyName("Paging")]
    public PaginationDto Pagination { get; set; }

    [JsonPropertyName("TotaalAantalObjecten")]
    public int NumberOfProperties { get; set; }
}