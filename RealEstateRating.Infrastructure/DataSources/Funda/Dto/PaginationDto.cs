using System.Text.Json.Serialization;

namespace RealEstateRating.Infrastructure.DataSources.Funda.Dto;

public class PaginationDto
{
    [JsonPropertyName("AantalPaginas")]
    public int NumberOfPages { get; set; }

    [JsonPropertyName("HuidigePagina")]
    public int CurrentPage { get; set; }
}