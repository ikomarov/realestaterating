using System.Text.Json.Serialization;

namespace RealEstateRating.Infrastructure.DataSources.Funda.Dto;

public class PropertyDto
{
    [JsonPropertyName("GlobalId")]
    public long GlobalId { get; set; }

    [JsonPropertyName("Id")]
    public Guid Id { get; set; }
    
    [JsonPropertyName("MakelaarId")]
    public long RealEstateAgentId { get; set; }

    [JsonPropertyName("MakelaarNaam")]
    public string? RealEstateAgentName { get; set; }
}
