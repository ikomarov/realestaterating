using System.Net.Http.Json;
using System.Runtime.Serialization;
using RealEstateRating.Domain.Models;
using RealEstateRating.Infrastructure.DataSources.Funda.Dto;

namespace RealEstateRating.Infrastructure.DataSources.Funda;

public interface IFundaHttpClient
{
    Task<FundaResponseDto> LoadAsync(string city, OutdoorSpaceType type, int page, int pageSize);
}

public class FundaHttpClient : IFundaHttpClient
{
    private readonly HttpClient _httpClient;

    public FundaHttpClient(HttpClient httpClient)
    {
        _httpClient = httpClient;
    }

    public async Task<FundaResponseDto> LoadAsync(string city, OutdoorSpaceType type, int page, int pageSize)
    {
        var searchString = BuildSearchString(city, type);

        var queryParams = new Dictionary<string, string>()
        {
            { "type", "koop" },
            { "zo", searchString },
            { "page", page.ToString() },
            { "pageSize", pageSize.ToString() }
        };
        
        var uriBuilder = new UriBuilder()
        {
            Query = string.Join("&", queryParams.Select(kvp => $"{kvp.Key}={kvp.Value}"))
        };
        
        var result = await _httpClient.GetAsync(uriBuilder.Query);
        
        result.EnsureSuccessStatusCode();
        var data = await result.Content.ReadFromJsonAsync<FundaResponseDto>();
        if (data == null)
            throw new SerializationException("Failed to deserialize FundaResponseDto");

        return data;
    }

    private string BuildSearchString(string city, OutdoorSpaceType type)
    {
        var typeParam = ConvertOutDoorSpaceTypeParam(type);
        
        if (String.IsNullOrEmpty(typeParam))
            return $"/{city}/";
        
        return $"/{city}/{typeParam}/";
    }

    private string ConvertOutDoorSpaceTypeParam(OutdoorSpaceType type)
    {
        switch (type)
        {
            case OutdoorSpaceType.Balcony:
                return "balkon";
            case OutdoorSpaceType.Garden:
                return "tuin";
            case OutdoorSpaceType.RoofTerrace:
                return "dakterras";
            case OutdoorSpaceType.NotSelected:
                return "";
            default:
                throw new ArgumentOutOfRangeException(nameof(type));
        }
    }
}