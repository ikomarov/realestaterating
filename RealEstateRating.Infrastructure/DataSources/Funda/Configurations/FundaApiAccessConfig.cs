namespace RealEstateRating.Infrastructure.DataSources.Funda.Configurations;

public class FundaApiConfig
{
    public FundaApiConfig(FundaApiAccessConfig accessConfig, FundaApiUsageConfig? usageConfig = null)
    {
        AccessConfig = accessConfig;
        UsageConfig = usageConfig ?? new FundaApiUsageConfig();
    }
    
    public FundaApiAccessConfig AccessConfig { get; set; }
    public FundaApiUsageConfig UsageConfig { get; set; }
}

public class FundaApiAccessConfig
{
    public FundaApiAccessConfig(string baseUrl, string apiKey)
    {
        BaseUrl = baseUrl;
        ApiKey = apiKey;
    }
    public string BaseUrl { get; set; }
    public string ApiKey { get; set; }
    
    public Uri FundaUrl => new($"{BaseUrl.TrimEnd('/')}/{ApiKey}/");
}

public class FundaApiUsageConfig
{
    public int BatchSize { get; set; } = 20;
    public int PageSize { get; set; } = 25;
    public int WindowInSeconds { get; set; } = 3;
    public int PermitLimit { get; set; } = 5;
}