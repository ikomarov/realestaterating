using Microsoft.Extensions.Options;
using RealEstateRating.Domain.Exceptions;
using RealEstateRating.Domain.Interfaces;
using RealEstateRating.Domain.Models;
using RealEstateRating.Infrastructure.DataSources.Funda.Configurations;
using RealEstateRating.Infrastructure.DataSources.Funda.Dto;

namespace RealEstateRating.Infrastructure.DataSources.Funda;

public class FundaDataSource : IDataSource
{
    private readonly int _batchSize;
    private readonly int _pageSize;

    private readonly IFundaHttpClient _httpClient;

    public FundaDataSource(IFundaHttpClient httpClient, IOptionsSnapshot<FundaApiUsageConfig> usageConfig)
    {
        _httpClient = httpClient;
        _pageSize = usageConfig.Value.PageSize;
        _batchSize = usageConfig.Value.BatchSize;
    }

    public async IAsyncEnumerable<IEnumerable<RawPropertyData>> PullData(string city, OutdoorSpaceType type)
    {
        var currentPage = 0;
        var firstPage = 
            (await LoadBatch(currentPage, numberOfPages: 1, batchSize: 1, _pageSize, city, type)).First();
        var numberOfPages = firstPage.Pagination.NumberOfPages;
        ++currentPage;

        yield return firstPage.Properties.Select(Map).AsEnumerable();

        while (currentPage < numberOfPages)
        {
            var data = await LoadBatch(currentPage, numberOfPages, 
                _batchSize, _pageSize, city, type);
            currentPage += _batchSize;
            numberOfPages = data.Last().Pagination.NumberOfPages;
            
            yield return data.SelectMany(x => x.Properties).Select(Map).AsEnumerable();
        }
    }

    private async Task<FundaResponseDto[]> LoadBatch(int currentPage, int numberOfPages, int batchSize,
        int pageSize, string city,
        OutdoorSpaceType type)
    {
        var loadBatchTask = Task.WhenAll(Enumerable.Range(currentPage + 1, numberOfPages - currentPage)
            .Take(batchSize)
            .Select(page => _httpClient.LoadAsync(city, type, page, pageSize)));
        
        try 
        {
            return await loadBatchTask;
        }
        catch (Exception e)
        {
            if (loadBatchTask.Exception != null)
                throw new DataLoadingException(loadBatchTask.Exception.InnerExceptions);

            throw new DataLoadingException(new []{ e });
        }
    }

    private RawPropertyData Map(PropertyDto dto) => new(dto.Id, dto.RealEstateAgentId, dto.RealEstateAgentName);
}