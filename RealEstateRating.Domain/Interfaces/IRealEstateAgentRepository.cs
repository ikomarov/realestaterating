using RealEstateRating.Domain.Models;

namespace RealEstateRating.Domain.Interfaces;

public interface IRealEstateAgentRepository
{
    void Add(string city, OutdoorSpaceType type, RealEstateAgent agent);
    void AddRange(string city, OutdoorSpaceType type, IEnumerable<RealEstateAgent> agents);
    IEnumerable<RealEstateAgent> GetAll(string city, OutdoorSpaceType type);
    bool TryGet(string city, OutdoorSpaceType type, long id, out RealEstateAgent? agent);
    bool TryGetAll(string city, OutdoorSpaceType type, out IEnumerable<RealEstateAgent> data);
}