using RealEstateRating.Domain.Models;

namespace RealEstateRating.Domain.Interfaces;

public interface IDataAnalyzer
{
    public IReadOnlyCollection<RealEstateAgent> TopByAmountOfProperties(int amount,
        IEnumerable<RealEstateAgent> data);
}