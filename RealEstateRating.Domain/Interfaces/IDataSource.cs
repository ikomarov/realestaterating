using RealEstateRating.Domain.Models;

namespace RealEstateRating.Domain.Interfaces;

public interface IDataSource
{
    /// <summary>
    /// Load data in batches from api
    /// </summary>
    /// <param name="city">City for which we want to load data</param>
    /// <param name="type">OutdoorSpaceType of property which we want to load</param>
    /// <returns>On each iteration returns next batch of loaded data</returns>
    public IAsyncEnumerable<IEnumerable<RawPropertyData>> PullData(string city, OutdoorSpaceType type);
}