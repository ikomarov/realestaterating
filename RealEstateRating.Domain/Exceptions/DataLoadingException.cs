namespace RealEstateRating.Domain.Exceptions;

public class DataLoadingException(IEnumerable<Exception> exceptions)
    : AggregateException($"Failed to load data exception", exceptions);