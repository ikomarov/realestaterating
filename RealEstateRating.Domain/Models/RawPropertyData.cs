namespace RealEstateRating.Domain.Models;

public class RawPropertyData(Guid id, long realEstateAgentId, string? realEstateAgentName)
{
    public Guid Id { get; } = id;
    public long RealEstateAgentId { get; } = realEstateAgentId;
    public string? RealEstateAgentName { get; } = realEstateAgentName;
}