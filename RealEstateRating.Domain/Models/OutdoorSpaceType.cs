namespace RealEstateRating.Domain.Models;

public enum OutdoorSpaceType
{
    NotSelected = 0,
    Garden = 1,
    Balcony = 2,
    RoofTerrace = 3
}