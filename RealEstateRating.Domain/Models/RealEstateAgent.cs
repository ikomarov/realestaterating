namespace RealEstateRating.Domain.Models;

public class RealEstateAgent
{
    public long Id { get; }
    public string Name { get; }
    public int CountOfPropertiesOnSale => _propertyOnSale.Count;

    private readonly HashSet<Guid> _propertyOnSale;

    public RealEstateAgent(long id, string? name) :  this(id, name, Array.Empty<Guid>())
    {
    }
    
    public RealEstateAgent(long id, string? name, IEnumerable<Guid> propertyOnSale)
    {
        if (id == 0)
            throw new ArgumentException($"{nameof(RealEstateAgent)}.{nameof(id)} can not be 0");
        
        Id = id;
        Name = String.IsNullOrEmpty(name) ? $"id: {id}" : name;

        _propertyOnSale = propertyOnSale.ToHashSet();
    }

    public void AddProperty(Guid id)
    {
        _propertyOnSale.Add(id);
    }
}