using RealEstateRating.Domain.Models;

namespace RealEstateRating.Application.UseCases.RealEstateAgentsTop;

public record RealEstateRatingDto(IReadOnlyCollection<RealEstateAgent> Data, long? AmountOfProcessedRecords = null);