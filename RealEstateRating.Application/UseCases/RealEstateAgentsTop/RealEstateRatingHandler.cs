using RealEstateRating.Application.DataLoading;
using RealEstateRating.Domain.Interfaces;
using RealEstateRating.Domain.Models;

namespace RealEstateRating.Application.UseCases.RealEstateAgentsTop;

public interface IRealEstateRatingHandler
{
    IAsyncEnumerable<RealEstateRatingDto> Handle(string city, OutdoorSpaceType type);
}

public class RealEstateRatingHandler : IRealEstateRatingHandler
{
    private const int TopLength = 10;
    
    private readonly IDataIngestionService _dataIngestionService;
    private readonly IRealEstateAgentRepository _repository;
    private readonly IDataAnalyzer _dataAnalyzer;

    public RealEstateRatingHandler(IDataIngestionService dataIngestionService,
        IRealEstateAgentRepository repository, IDataAnalyzer dataAnalyzer)
    {
        _dataIngestionService = dataIngestionService;
        _repository = repository;
        _dataAnalyzer = dataAnalyzer;
    }

    public async IAsyncEnumerable<RealEstateRatingDto> Handle(string city, OutdoorSpaceType type)
    {
        if (_repository.TryGetAll(city, type, out var data))
        {
            yield return new RealEstateRatingDto(_dataAnalyzer.TopByAmountOfProperties(TopLength, data));
            yield break;
        }
        
        await foreach (var amountOfProcessedRecords in  _dataIngestionService.IngestData(city, type))
        {
            var top = _dataAnalyzer.TopByAmountOfProperties(TopLength, _repository.GetAll(city, type));
            yield return new RealEstateRatingDto(top, amountOfProcessedRecords);
        }
    }
}