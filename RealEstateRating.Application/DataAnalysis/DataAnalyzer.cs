using RealEstateRating.Domain.Interfaces;
using RealEstateRating.Domain.Models;

namespace RealEstateRating.Application.DataAnalysis;

public class DataAnalyzer : IDataAnalyzer
{
    public IReadOnlyCollection<RealEstateAgent> TopByAmountOfProperties(int amount, IEnumerable<RealEstateAgent> data)
    {
        return Top(amount, data, agent => agent.CountOfPropertiesOnSale);
    }

    // Of course could be just sorting and taking first k elements,
    // but this approach gives us O(N * Log(k)) time complexity instead of O(N * Log(N)) and it's still quite simple
    private IReadOnlyCollection<T> Top<T>(int amount, IEnumerable<T> data, Func<T, int> prioritySelector)
    {
        var queue = new PriorityQueue<T, int>();
        foreach (var item in data)
        {
            queue.Enqueue(item, prioritySelector(item));

            if (queue.Count > amount)
                queue.Dequeue();
        }

        var result = new T[queue.Count];
        for (int i = result.Length - 1; i >= 0; --i)
            result[i] = queue.Dequeue();

        return result;
    }
}