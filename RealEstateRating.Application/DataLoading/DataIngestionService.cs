using RealEstateRating.Domain.Interfaces;
using RealEstateRating.Domain.Models;

namespace RealEstateRating.Application.DataLoading;

public interface IDataIngestionService
{
    /// <summary>
    /// Load data from data source and save it to repository
    /// Each iteration of IAsyncEnumerable load and save new batch of data
    /// </summary>
    /// <returns>On each iteration returns amount of so far processed records</returns>
    IAsyncEnumerable<long> IngestData(string city, OutdoorSpaceType type);
}

public class DataIngestionService : IDataIngestionService
{
    private readonly IRealEstateAgentRepository _repository;
    private readonly IDataSource _dataSource;

    public DataIngestionService(IRealEstateAgentRepository repository, IDataSource dataSource)
    {
        _repository = repository;
        _dataSource = dataSource;
    }

    public async IAsyncEnumerable<long> IngestData(string city, OutdoorSpaceType type)
    {
        long amountOfProcessedRecords = 0;
        await foreach (var batch in _dataSource.PullData(city, type))
        {
            foreach (var item in batch)
            {
                ++amountOfProcessedRecords;
                if (_repository.TryGet(city, type, item.RealEstateAgentId, out var agent) && agent != null)
                    agent.AddProperty(item.Id);
                else
                    _repository.Add(city, type, new RealEstateAgent(item.RealEstateAgentId, item.RealEstateAgentName, new[] { item.Id }));
            }
            
            // In case of usage of not in-memory storage call to 'SaveChanges' is needed here

            yield return amountOfProcessedRecords;
        }
    }
}