

using Microsoft.Extensions.DependencyInjection;
using RealEstateRating.Application.DataAnalysis;
using RealEstateRating.Application.DataLoading;
using RealEstateRating.Application.UseCases.RealEstateAgentsTop;
using RealEstateRating.Domain.Interfaces;

namespace RealEstateRating.Application;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddApplication(this IServiceCollection services)
    {
        services.AddTransient<IDataIngestionService, DataIngestionService>();
        services.AddTransient<IDataAnalyzer, DataAnalyzer>();
        services
            .AddTransient<IRealEstateRatingHandler,
                RealEstateRatingHandler>();

        return services;
    }
}
